Detector Frame (`ligo.skymap.postprocess.detector_frame`)
=========================================================

This module is **deprecated**. Please use
:mod:`ligo.skymap.coordinates.detector` instead.

.. automodule:: ligo.skymap.postprocess.detector_frame
    :members:
    :show-inheritance:
