BAYESTAR Rapid Localization (`ligo.skymap.bayestar`)
====================================================

.. automodule:: ligo.skymap.bayestar
    :members:
    :show-inheritance:
